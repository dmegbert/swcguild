//runs all the validate functions
function validateAll() {
	var z = 0;
	if (validateName() == false){
		z++;
	}
	if (validateEmailPhone() == false) {
		z++;
	}
	if (validateAdditional() == false) {
		z++;
	}
	if (validateBestDay() == false) {
		z++;
	}
	if (z > 0) {
		return false;
	}
}

//validate a name is entered
function validateName() {
    var a = document.forms["contactForm"]["name"].value;
    if (a == null || a == "") {
		document.getElementById('nameError').innerHTML = "Please enter your name";
		return false;
    }
}

//validate an email address or phone number is entered 
function validateEmailPhone() {
    var b = document.forms["contactForm"]["email"].value;
    var c = document.forms["contactForm"]["phone"].value;
	if ((b == null || b == "") && (c == null || c == "")) {
		document.getElementById('comError').innerHTML = "Please enter your phone number or email";
		return false;
    }
}

//validate additional info text is entered if other is selected from 'Reason for Inquiry'
function validateAdditional() {
	var d = document.forms["contactForm"]["reason"].value;
	var e = document.forms["contactForm"]["additionalInfo"].value;
	if (d == "other" && (e == null || e == "")) {
		document.getElementById('additionalError').innerHTML = "Please enter a reason for your inquiry in the \"Additional Information\" section below.";	
		return false;
	}
}

function validateBestDay() {
    var i;
	for (i = 0; i < 5; i++) {
		if (contactForm.bestDay[i].checked == true) {
			return true;
		}
	}
	document.getElementById('dayError').innerHTML = "Please select the day\(s\) we should contact you";	
	return false;
}