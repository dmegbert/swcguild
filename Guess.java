class Guess {
	public static void main(String args[]) 
	throws java.io.IOException {
		
		char ch, ignore, answer = 'T';
		int counter = 0;
		game:	{	do {
		System.out.println("I'm thinking of a letter between A and Z.");
		System.out.print("Can you guess it: ");
		counter++;
		ch = (char) System.in.read();
		
		
				do {
				ignore = (char) System.in.read();
				} while (ignore != '\n');
		if (ch <= 'z' & ch >= 'a') System.out.println("Your guess must be upper case!\nTry again!");
		else {
			if(ch == answer) System.out.println("***Right!***");
			else {
				if (counter == 2) System.out.println ("Sean, I'm disappointed in you.");
				if (counter >=5 & counter <= 8) System.out.println ("You're getting there.");
				if (counter > 15) break game;
				System.out.print("Sorry you are ");
				if (ch < answer) System.out.println("not far enough");
				else System.out.println("too far");
				System.out.println("Try Again!\n");
				}
			}
		} while (answer != ch);
		} System.out.println("Game over.");
	}	
}
