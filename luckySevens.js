
//declare variables for player's bet, die one, die two, max money, 
var bet, sBet, x, y, maxCash, maxCashTurns, turns;

//roll the virtual dice when the player clicks on the "Roll the Dice!" button
function playGame() {
bet = document.getElementById("bet").value;
bet = parseInt(bet);
sBet = bet;
console.log(bet);
maxCash = bet;
maxCashTurns = 1;
if (bet > 0 && bet != NaN) {
	for (turns=1; bet > 0; turns++) {
		die1();
		die2();
		var roll = x + y;
		if (roll == 7) {
			bet += 4;
		}
		else {
			bet -= 1;
		}
		if (bet > maxCash) {
				maxCash = bet;
				maxCashTurns = turns;
			}
		console.log(turns);
		showRolling('rollingDice');
		//document.getElementById("pot").innerHTML = bet;
	}
	document.getElementById('startBet').innerHTML = "$" + sBet;
	document.getElementById('totalRolls').innerHTML = turns;
	document.getElementById('maxCashWon').innerHTML = "$" + maxCash;
	document.getElementById('maxRollCount').innerHTML = maxCashTurns;
	document.getElementById('roll').value = "Roll Again!"
	delayShow('results');
}
else {
	alert("The bet must be larger than 0 and a number (10, 150, etc.)");
}
console.log(maxCash);
console.log(maxCashTurns);

}
//sets die one to a random number between 1 and 7
function die1() {
    x = Math.floor((Math.random() * 6) + 1);
	return x;
}
//sets die two to a random number between 1 and 7
function die2() {
    y = Math.floor((Math.random() * 6) + 1);
	return y;
}
//shows the results table
function show(results){
   document.getElementById("results").style.visibility="visible";
}
//delay showing result table until dice hand disappears
function delayShow(results) {
	hide('results');
	setTimeout("show()", 951);
}
//hides the results table
function hide(results){
   document.getElementById("results").style.visibility="hidden";
}
//shows and then hides rolling dice animated gif
function showRolling(rollingDice){
   document.getElementById("rollingDice").src = "http://garysanis.com/Pics/Games/HandRollingDice7_Big.gif";
   document.getElementById("rollingDice").style.visibility="visible";
   setTimeout("hideRolling()", 950); //hides animated gif after .95 seconds
}
 
function hideRolling(rollingDice){
   document.getElementById("rollingDice").src = "";
   document.getElementById("rollingDice").style.visibility="hidden";
}

/*functions I made before I read the pdf file with the detailed directions. oops.
function delayDice(dice) {
	document.getElementById("dice").style.visibility="hidden";
	setTimeout("show()", 951);
}

function show(dice){
   document.getElementById("dice").style.visibility="visible";
}

function hide(dice){
   document.getElementById("dice").style.visibility="hidden";
}

function changeDieOne() {
    document.getElementById("dieOneImage").src = "die_0"+x+".gif";
}

function changeDieTwo() {
    document.getElementById("dieTwoImage").src = "die_0"+y+".gif";
}
*/	