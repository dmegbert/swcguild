class Vehicle {
	int passengers;  //number of passengers
	int fuelcap; 	//fuel capacity in gallons
	int mpg;	//fuel consumption in miles per gallon 
	int range() {
		return fuelcap * mpg;	//a method for the range
	}
	double fuelneeded(int miles) {	//a method that needs a parameter
		return (double) miles / mpg;
	}
}

class CompFuel {
	public static void main(String args[]) {
		Vehicle minivan = new Vehicle();
		Vehicle sporty = new Vehicle();
		int dist = 252;
		
		minivan.passengers = 7;
		minivan.fuelcap = 16;
		minivan.mpg = 21;
		
		
		sporty.passengers = 2;
		sporty.fuelcap = 20;
		sporty.mpg = 25;
		
		 
		System.out.println("To go " + dist + " miles.  Minivan needs " + minivan.fuelneeded(dist) + " gallons of fuel\n");
		
		System.out.println("\nMinivan can carry " + minivan.passengers + " with a range of " + minivan.range() + "\n");
		
		System.out.println("To go " + dist + " miles. Sporty needs " + sporty.fuelneeded(dist) + " gallons of fuel");
		
		System.out.print("\nSporty can carry " + sporty.passengers + " with a range of " + sporty.range() + "\n");
	}
}