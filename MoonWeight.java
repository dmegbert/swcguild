class MoonWeight {
	public static void main (String args[]) {
		double earth_lb, moon_lb;
		earth_lb = 175;
		moon_lb = earth_lb * .17;
		System.out.println("My weight on Earth is " + earth_lb);
		System.out.println();
		System.out.println("My weight on the Moon is " + moon_lb);
	}
}
	