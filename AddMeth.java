class Vehicle {
	int passengers;  //number of passengers
	int fuelcap; 	//fuel capacity in gallons
	int mpg;	//fuel consumption in miles per gallon 
	
	//Display the range
	void range() {
		System.out.println("Range is " + fuelcap * mpg);
	}
}

class AddMeth {
	public static void main(String args[]) {
		Vehicle minivan = new Vehicle();
		Vehicle sporty = new Vehicle();
		//int range1, range2;
		
		minivan.passengers = 7;
		minivan.fuelcap = 16;
		minivan.mpg = 21;
		
		sporty.passengers = 2;
		sporty.fuelcap = 20;
		sporty.mpg = 25;
		
		
		System.out.println("Minivan can carry " + minivan.passengers + ".");
		
		minivan.range(); //prints out minivan range
		
		System.out.print("\nSporty can carry " + sporty.passengers + ".");
		
		sporty.range();  //prints out sporty's range 
	}
}