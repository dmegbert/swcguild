class Vehicle {
	int passengers;  //number of passengers
	int fuelcap; 	//fuel capacity in gallons
	int mpg;	//fuel consumption in miles per gallon 
	
	//Display the range
	int range() {
		return fuelcap * mpg;
	}
}

class RetMeth {
	public static void main(String args[]) {
		Vehicle minivan = new Vehicle();
		Vehicle sporty = new Vehicle();
		int range1, range2;
		
		minivan.passengers = 7;
		minivan.fuelcap = 16;
		minivan.mpg = 21;
		range1 = minivan.range();
		
		sporty.passengers = 2;
		sporty.fuelcap = 20;
		sporty.mpg = 25;
		//range2 = sporty.range();
		
		
		System.out.println("\nMinivan can carry " + minivan.passengers + " with a range of " + range1);
		
		
		
		System.out.print("\nSporty can carry " + sporty.passengers + " with a range of " + sporty.range() + "\n");
	}
}