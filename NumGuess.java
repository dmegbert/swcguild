import java.util.Random;
import java.util.Scanner;
class NumGuess {
	public static void main(String args[]) 
	throws java.io.IOException {
		
		
		int answer, ch, counter = 0;
		Random randomGenerator = new Random();
		int rand = randomGenerator.nextInt(100);
		answer = rand;
		Scanner in = new Scanner(System.in);
		game:	{	do {
		System.out.println("I'm thinking of a number between 0 and 100.");
		System.out.print("Can you guess it: ");
		counter++;
		ch = in.nextInt();
			
			if(ch == answer) System.out.println("***Right!***");
			else {
				int abs = Math.abs(answer - ch);
				if (abs > 50) System.out.println ("Not even close!!!");
				if (abs < 10) System.out.println ("You're getting there.");
				if (counter > 15) break game;
				System.out.println();
				System.out.print(" Sorry you're guess is ");
				if (ch < answer) System.out.println("too small");
				else System.out.println("too big");
				System.out.println("Try Again!\n");
				}
		} while (answer != ch);
		} System.out.println("Game over.");
	}	
}