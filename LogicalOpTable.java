class LogicalOpTable {
	public static void main(String args[]) {
		boolean p, q;
		int tf;
		
		System.out.println("P\tQ\tAND\tOR\tXOR\tNot");
		
		p = true; q = true;
		System.out.print(p + "\t" + q + "\t");
		System.out.print((p&q) + "\t" + (p|q) + "\t");
		System.out.println((p^q) + "\t" + (!p));
		
		if (p) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!q) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p&q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p&q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p|q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p|q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p^q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p^q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 1;
			System.out.println(tf + "\t");
		}
		if (p) {
			tf = 0;
			System.out.println(tf + "\t");
		}
		
		p = true; q = false;
		System.out.print(p + "\t" + q + "\t");
		System.out.print((p&q) + "\t" + (p|q) + "\t");
		System.out.println((p^q) + "\t" + (!p));
		
		if (p) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!q) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p&q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p&q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p|q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p|q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p^q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p^q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 1;
			System.out.println(tf + "\t");
		}
		if (p) {
			tf = 0;
			System.out.println(tf + "\t");
		}
		
		p = false; q = true;
		System.out.print(p + "\t" + q + "\t");
		System.out.print((p&q) + "\t" + (p|q) + "\t");
		System.out.println((p^q) + "\t" + (!p));
		
		if (p) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!q) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p&q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p&q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p|q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p|q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p^q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p^q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 1;
			System.out.println(tf + "\t");
		}
		if (p) {
			tf = 0;
			System.out.println(tf + "\t");
		}
		
		p = false; q = false;
		System.out.print(p + "\t" + q + "\t");
		System.out.print((p&q) + "\t" + (p|q) + "\t");
		System.out.println((p^q) + "\t" + (!p));
		
		
		if (p) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!q) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p&q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p&q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p|q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p|q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (p^q) {
			tf = 1;
			System.out.print(tf + "\t");
		}
		if (!(p^q)) {
			tf = 0;
			System.out.print(tf + "\t");
		}
		if (!p) {
			tf = 1;
			System.out.println(tf + "\t");
		}
		if (p) {
			tf = 0;
			System.out.println(tf + "\t");
		}
	}
}